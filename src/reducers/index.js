import { ADD_NOTE, EDIT_NOTE, DELETE_NOTE } from "../constants"

const note = (action) => {
    let { title, body } = action;

    if(action.type == ADD_NOTE) {

        return {
            id: Math.random(),
            title,
            body
        }
    }

    let { id } = action;

    return {
        id,
        title,
        body
    }

}

const deleteNoteById = (state = [], id) => {
    const notes = state.filter(note => note.id != id)
    return notes;
}

const notes = (state = [], action) => {
    let notes = null;

    switch(action.type) {
        case ADD_NOTE:
            notes = [...state, note(action)];
            return notes
        case EDIT_NOTE:
            notes = [...state, note(action)];
            return notes;
        case DELETE_NOTE:
            notes = deleteNoteById(state, action.id);
            return notes;
        default:
            return state;
    }
}

export default notes;