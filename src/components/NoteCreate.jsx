import React, { Component } from 'react';
import { Form, FormGroup, FormControl, ControlLabel, Button, Glyphicon } from 'react-bootstrap';

class NoteCreate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            body: ''
        }
    }

    render() {
        return (
            <div className="create-note">
                <div className="clearfix">
                    <Button className="custom-create-button pull-right" type="submit"><Glyphicon className="custom-plus-glyphicon" glyph="glyphicon glyphicon-plus"/> Add Note</Button>
                </div>
                <Form>
                    <FormGroup controlId="formControlsTitle">
                        <ControlLabel>Title : </ControlLabel>
                        <FormControl type="text" placeholder="Enter title"/>
                    </FormGroup>
                    <FormGroup controlId="formControlsBody">
                        <ControlLabel>Body : </ControlLabel>
                        <FormControl componentClass="textarea" placeholder="Enter Body"/>
                    </FormGroup>

                    <Button className="pull-right" bsStyle="primary" type="submit">Save</Button>
                </Form>
            </div>

        )
    }
}

export default NoteCreate;