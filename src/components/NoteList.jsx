import React, { Component } from 'react';
import { ListGroup, ListGroupItem, Glyphicon } from 'react-bootstrap';

class NoteList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ListGroup>
                <ListGroupItem><h4>Hello <Glyphicon className="custom-glyphicon" glyph="glyphicon glyphicon-remove"/></h4><span className="subtitle">Description</span></ListGroupItem>
                <ListGroupItem>World <Glyphicon className="custom-glyphicon" glyph="glyphicon glyphicon-remove"/></ListGroupItem>
                <ListGroupItem>React <Glyphicon className="custom-glyphicon" glyph="glyphicon glyphicon-remove"/></ListGroupItem>
                <ListGroupItem>IS <Glyphicon className="custom-glyphicon" glyph="glyphicon glyphicon-remove"/></ListGroupItem>
                <ListGroupItem>Superb <Glyphicon className="custom-glyphicon" glyph="glyphicon glyphicon-remove"/></ListGroupItem>
            </ListGroup>
        )
    }
}

export default NoteList;