import { ADD_NOTE, EDIT_NOTE, DELETE_NOTE, LIST_NOTE } from "../constants";

export const addNote = (title, body) => {
    const action = {
        type: ADD_NOTE,
        title,
        body
    }

    return action;

}

export const editNote = (id, title, body) => {
    const action = {
        type: EDIT_NOTE,
        id,
        title,
        body
    }

    return action;
}

export const deleteNote = (id) => {
    const action = {
        type: DELETE_NOTE,
        id
    }

    return action;
}

export const listNotes = () => {
    const action = {
        type: LIST_NOTE
    }

    return action;
}