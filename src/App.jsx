import React, { Component } from 'react';
import NoteList from './components/NoteList';
import NoteCreate from './components/NoteCreate';

class App extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div className="wrap">
            <div className="note">
                <div className="note-heading clearfix">
                    <div className="col-md-12 ">
                        G Notes
                    </div>
                </div>

                    <div className="col-md-4 col-sm-4 pl-0 pr-0">
                        <div className="note-list">
                            <NoteList />
                        </div>
                    </div>
                    <div className="col-md-8 col-sm-8">
                        <div className="note-create">
                            <NoteCreate />
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default App;